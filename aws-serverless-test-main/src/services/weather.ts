import * as moment from 'moment';
const http = require('http);
export const getWeather = async (options): Promise<any> => {


  try {
    http.get(options, function (response) {
      var body = '';

      response.on('data', function (chunk) {
        body += chunk;
      });

      response.on('end', function () {
        return JSON.parse(body);
      });
    }).on('error', function (e) {
      console.log("Error: ", e);
    });

  } catch(error){
    throw new Error('Downstream Error');
  }


};

module.exports = {

  getWeather

}
