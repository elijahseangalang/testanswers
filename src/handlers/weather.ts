import { returnResponse } from "./return";
const queryString = require ('querystring');

export const myhandler =  async (event) => {
  // this is my code, this is my destiny\

  let getWeatherResponse;
  const apiKey = process.env.API_KEY;

  try{

    const{ postCode, countryCode} = event.body;


    if (postCode || countryCode) {

      const parameters = {

        zip: postCode + ',' + countryCode,
        appid:apiKey
      }

      const getRequestParams = queryString.stringify(parameters)

      const options = {

        url: 'https://api.openweathermap.org',
        path: '/data/2.5/weather?' + getRequestParams,
        headers: {
        'Content-Type': 'application/json'
      }
    }

    getWeatherResponse = await weatherAPIRepo.getWeather(options);

    } else {
      throw new Error('Missing Parameters');
    }

  } catch(error){

    console.log(error);

  }

  const result = {
    lon: getWeatherResponse.coord.lon,
    lat: getWeatherResponse.coord.lat,
    main: getWeatherResponse.weather[0].main,
    description: getWeatherResponse.weather[0].description,
    temp: getWeatherResponse.main.temp,
    feels_like: getWeatherResponse.main.feels_like,
    temp_min: getWeatherResponse.main.temp_min,
    temp_max: getWeatherResponse.main.temp_max,
    pressure: getWeatherResponse.main.pressure,
    humidity: getWeatherResponse.main.humidity
  }

  return returnResponse({

    statusCose: 200,
    result

  });
}

export const handler = myhandler;
